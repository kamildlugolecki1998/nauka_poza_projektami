﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagerZadan : MonoBehaviour
{
    [SerializeField] Button[] przyciskiOdpowiedzi;
    [SerializeField] Text txtTrescZadania;
    [SerializeField] Zadanie[] zadania;
    int numerZadania = 0;
    string odpowiedzUzytkownika = "";
    void Start()
    {
    }

  
    void WyswietlZadania()
    {
        txtTrescZadania.text = zadania[0].trescZadania;
        przyciskiOdpowiedzi[0].GetComponentInChildren<Text>().text = zadania[numerZadania].PrawidlowaOdpowiedz;
        przyciskiOdpowiedzi[1].GetComponentInChildren<Text>().text = zadania[numerZadania].zlaOdpowiedz1;
        przyciskiOdpowiedzi[2].GetComponentInChildren<Text>().text = zadania[numerZadania].zlaOdpowiedz2;
        txtTrescZadania.text = zadania[numerZadania].trescZadania + odpowiedzUzytkownika;
    }

    public void PodajOdpowiedz(string odpowiedz)
    {
        odpowiedzUzytkownika = odpowiedz;
        WyswietlZadania();
    }

    public void SprawdzOdp()
    {
        if (odpowiedzUzytkownika == zadania[numerZadania].PrawidlowaOdpowiedz)
        {
            print("gooooood");
            numerZadania++;
            WyswietlZadania();
        }
        else
        {
            print("nie good");
        }
    }
}
