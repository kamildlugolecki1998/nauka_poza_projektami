﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Draggable : MonoBehaviour
{
    [SerializeField] string objName;
    Collider2D thisColider;
    Camera camMain;
    bool isBeingDragged;
    bool canBeDragged;
    void Start()
    {
        canBeDragged = true;
        isBeingDragged = false;
        thisColider = GetComponent<Collider2D>();
        camMain = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePosition = camMain.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButton(0) && canBeDragged)
        {
            Collider2D coll = Physics2D.OverlapPoint(mousePosition);
            if (coll == thisColider)
            {
                isBeingDragged = true;
            }

            if (isBeingDragged)
            {
                transform.position = mousePosition;
            }

        }else
        {
            isBeingDragged = false;
        }
        if (Input.GetMouseButtonUp(0))
        {
            canBeDragged = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        transform.position = collision.transform.position;
        canBeDragged = false;
        if (objName == collision.GetComponent<DraggableSpot>().GetName())
        {
            print("Succes");
        }
        else
        {
            print("Wrong object");
        }
    }
}
