﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


 [CreateAssetMenu(fileName ="Zadanie",menuName = "Utworz nowe zadanie")]
public class Zadanie : ScriptableObject
{
    public string trescZadania;
    public string PrawidlowaOdpowiedz;
    public string zlaOdpowiedz1;
    public string zlaOdpowiedz2;

}
